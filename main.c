#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <winsock2.h>
#include <dirent.h>
#include <windows.h>
#include <winbase.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <share.h>
#include <errno.h>
#include <string.h>

/*CLIENTV2*/

void envoiFichier(char const * const src, SOCKET sock)
{

    FILE* fSrc;
    char bloc[1024];
    char strTaille[5];
    int taille;

    char etat[3] = "OK";
    char stop[3] = "KO";

    if ((fSrc = fopen(src, "rb")) == NULL)
        printf("Erreur ouverture fichier\n");

    send(sock, etat, sizeof(etat),0);

    printf("Envoi en cours ");
    int d = 0;
    while ((taille = fread(bloc, 1, 1024, fSrc)) != 0)
    {
        if(d%20000 == 0)
            printf(".");
        d++;
        send(sock, etat, sizeof(etat),0);
        send(sock, bloc, sizeof(bloc),0);
        sprintf(strTaille, "%d",taille);
        send(sock, strTaille, sizeof(strTaille),0);
    }
    send(sock, stop, sizeof(stop),0);
    printf("\nEnvoi termine\n");
    printf("Hello Bitbucket!");
    fclose(fSrc);
}

void receptionfic(char const * const nomfic, SOCKET sock)
{
    FILE* filesource;
    char donnees[1024];
    char strTaille[5];
    int taille;
    int etat = 0;
    char path[300];
    char nomficfinal[1024];
    int len = strlen(nomfic);
    int i = 0;
   /* while(i< len-1){
        nomficfinal[i]=nomfic[i];
        i++;
    }*/
    strcpy(path,"./ficclient/");
    strcat(path, nomfic);//on add le chemin vers le dossier fic
    path[strlen(path)+1] = '\0';
    char etatstr[3];
    recv(sock, etatstr, sizeof(etatstr),0);
    strtok(path, "\n");
    printf("PATH : %s\n", path);

    if(strcmp(etatstr,"OK") == 0)//init pour savoir si tout est bon du cot� du serveur
    {
        etat = 1;
        filesource = fopen(path, "wb");
        if(filesource == NULL)
        {
            printf("\nErreur lors de l'ouverture du fichier!\n");
            etat = 0;
        }
    }
    else
    {
        etat = 0;
        printf("\nEchec de la reception du fichier!");
    }
    if(etat == 1){
        printf("Debut de la reception du fichier!\n");
    }
    while(etat == 1)
    {
        recv(sock, etatstr, sizeof(etatstr),0);
        if(strcmp(etatstr,"OK") == 0)
        {
            recv(sock, donnees, sizeof(donnees),0);
            recv(sock, strTaille, sizeof(strTaille),0);
            taille = strtol(strTaille, NULL, 10); //convertion str to int
            fwrite(donnees, 1, taille, filesource);//ecriture dans le fichier @param texte+tailletexte
        }
        else
        {
            etat = 0;

            printf("\nFichier recu avec succes!\n");
        }

    }
     fclose(filesource);
}

int main()
{
    WSADATA WSAData;
    SOCKET sock;
    SOCKADDR_IN sin;
    SOCKET mysock;
    SOCKADDR_IN mysockin;
    const char buffer[255];

    //////////R�cup�ration des donn�es de l'utilisateur/////////////////

    char getip[15];
    printf("Veuillez entrer l'adresse IP du serveur : ");
    fflush(stdin); // vider le buffer et accepter prochaine valeur
    fgets(getip, sizeof(getip),stdin);//enregistrement de la val : (nom de la variable, taille max de la variable, pointer vers la var de stream)
    char charport[5];
    int port;
    printf("Veuillez entrer le numero du port : ");
    fflush(stdin); // vider le buffer et accepter prochaine valeur
    fgets(charport, sizeof(charport),stdin);//enregistrement de la val : (nom de la variable, taille max de la variable, pointer vers la var de stream)
    port = atoi(charport);


//////////////////////creation du socket*/////////////////

    WSAStartup(MAKEWORD(2,0), &WSAData);// initialisation du socket (makeword(version winsock), variable WSADATA)
    sock = socket(AF_INET, SOCK_STREAM, 0);//cr�ation socket (typeSocket, ouverture connection, 0=default protocol(TCP))
    sin.sin_addr.s_addr = inet_addr(getip);//adresse serveur
    sin.sin_family = AF_INET;//type de socket (protocole internet IPv4)
    sin.sin_port = htons(port);//num du port telnet (htons converts to network byte : different order big enndian vs little enndian)

    ////////////////Connection au serveur///////////////////

    int val = connect(sock, (SOCKADDR *)&sin, sizeof(sin));//conection au server avec (socket, structure socket, taille socket)
    if(val != 0)
    {
        printf("\nConnection echouee! Veuillez verifier l'adresse Ip et le numero de port du serveur! \n");
    }
    else
    {
        printf("\nClient connecte au serveur!\n\n");
        int connected = 1;
        char action[5];
        while(connected == 1){

            int initaction = 1;
            while(strcmp(action,"GET\n") != 0 && strcmp(action,"get\n") != 0 && strcmp(action,"LIST") != 0 && strcmp(action,"list") != 0 && strcmp(action,"QUIT") != 0 && strcmp(action,"quit") != 0 && strcmp(action,"PUT\n") != 0 && strcmp(action,"put\n") != 0) //detection d'erreur lors de l'entr�e de l'action � executer
            {
                if(initaction != 1)//si c'est la premi�re tentative, on affiche pas de msg d'erreur
                {
                    printf("\nCommande invalide!\n");
                }
                initaction = 0;
                printf("\nQuelle action souhaitez-vous executer ? (Recevoir la liste des fichiers disponibles [LIST] / Recevoir un fichier [GET] /Envoyer un fichier [PUT] / Quitter [QUIT]) : ");
                fflush(stdin); // vider le buffer et accepter prochaine valeur
                fgets(action,sizeof(action),stdin);//convertion de ASCII
            }
            if(strcmp(action,"LIST") == 0 || strcmp(action,"list") == 0)//action: afficher liste
            {
                char msg[5] = "LIST";
                send(sock,msg,sizeof(msg),0);
                char repnb[5];
                int nbfic;
                recv(sock,repnb,sizeof(repnb),0);
                nbfic = atoi(repnb);//str to int

                char nomfic[255];
                int i = 0;
                printf("\n");
                while(i<nbfic)
                {
                    recv(sock,nomfic,sizeof(nomfic),0);//reception des noms de fichiers 1 par 1
                    printf("\n%s\n",nomfic);
                    i++;
                }
                action[0] = '\0';//on vide la valeur de action

            }
            if(strcmp(action,"GET\n") == 0 || strcmp(action,"get\n") == 0)//action : recevoir fichier
            {
                char nomfic[1024];
                char msg[8] = "GETFILE";
                send(sock,msg,sizeof(msg),0);
                printf("Veuilez rentrer le nom de votre fichier : ");
                fflush(stdin); // vider le buffer et accepter prochaine valeur
                fgets(nomfic, sizeof(nomfic),stdin);
                int len = strlen(nomfic);
                //nomfic[len] = '\0';
                send(sock,nomfic,sizeof(nomfic),0);
                receptionfic(nomfic,sock);
                action[0] = '\0';//on vide la valeur de action
            }
            if(strcmp(action,"quit") == 0 || strcmp(action,"QUIT") == 0)//action : quitter
            {
                char msg[5] = "DECO";
                send(sock,msg,sizeof(msg),0);
                closesocket(sock);
                connected = 0;
                action[0] = '\0';//on vide la valeur de action
                printf("\nClient deconnecte!");
            }
            if(strcmp(action,"PUT\n") == 0 || strcmp(action,"put\n") == 0)//action : envoyer un fichier
            {
                char msg[8] = "PUTFILE";
                char nomfic[1024];
                char newnomfic[1024];
                char path[300];
                char nomficfinal[1024];
                send(sock,msg,sizeof(msg),0);
                printf("Veuilez rentrer le nom de votre fichier : ");
                fflush(stdin); // vider le buffer et accepter prochaine valeur
                fgets(nomfic, sizeof(nomfic),stdin);
                int len = strlen(nomfic);
                nomfic[len] = '\0';
                printf("Veuillez entrer le nom souhaite pour la copie de votre fichier (avec la bonne extension) : ");
                fflush(stdin); // vider le buffer et accepter prochaine valeur
                fgets(newnomfic, sizeof(newnomfic),stdin);
                int lennew = strlen(newnomfic);
                newnomfic[lennew] = '\0';
                send(sock,newnomfic,sizeof(newnomfic),0);
                strcpy(path,"./ficclient/");
                strcat(path, nomfic);//on add le chemin vers le dossier fic
                path[strlen(path)+1] = '\0';
                envoiFichier(path,sock);

                action[0] = '\0';//on vide la valeur de action
            }
        }




    }

    closesocket(sock);
    WSACleanup();//nettoyage du WSA
    return 0;
}
